﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// v1.00 - test GitLab
namespace FileSync
{
    class Program
    {
        static void Main(string[] args)
        {
            List<FileInfo> fileInfoList = new List<FileInfo>();

            IEnumerable<string> GetFilesFromDir(string dir) =>
             Directory.EnumerateFiles(dir).Concat(
             Directory.EnumerateDirectories(dir)
                      .SelectMany(subdir => GetFilesFromDir(subdir)));
            Console.WriteLine("Reading dir...");
            var fileList = GetFilesFromDir(@"v:\");
            Console.WriteLine($"Total files in folder: {fileList.Count()}");
            Console.WriteLine("Getting fileinfo...");
            foreach(string file in fileList)
            { // whatever
                var masterFile = new FileInfo(file);
                var slaveFileName = "e" + file.Substring(1);
                var slaveFile = new FileInfo(slaveFileName);

                Console.Write($"{masterFile.Name}");

                if (!slaveFile.Exists || 
                    //masterFile.CreationTime != slaveFile.CreationTime ||
                    masterFile.LastWriteTime > slaveFile.LastWriteTime)
                {
                    if (!slaveFile.Directory.Exists)
                        slaveFile.Directory.Create();

                    if (!slaveFile.Exists)
                        Console.WriteLine(" (Copied, not existing)");
                    else
                    //if (masterFile.CreationTime != slaveFile.CreationTime)
                    //    Console.WriteLine(" (Copied, creation date differs)");
                    if (masterFile.LastWriteTime > slaveFile.LastWriteTime)
                        Console.WriteLine(" (Copied, master is newer)");


                    masterFile.CopyTo(slaveFile.FullName, true);
                }
                else
                {
                    Console.WriteLine($" (Skipped, up to date)");
                }
                //fileInfoList.Add(fi);
                //Console.WriteLine($"{ fi.CreationTime.ToShortDateString()}");

            }
        }
    }
}
